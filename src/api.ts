import axios from "axios";

const api = axios.create({ baseURL: "http://localhost:8080/api/v1" });

export const LOGIN = "/auth/login";
export const REGISTER = "/user/register";
export const TEAM = "/team";
export const USER = "/user";
export const CURRENT_USER_TEAMS = "/team/currentUser";
export const ALL_USERS_IN_USER_TEAMS = "/user/teams/allUsers";
export const CHORE = "/chore";
export const STATISTICS = "/statistics";
export const CURRENT_USER = "/auth/current";
export const TEAM_SEARCH = "/team/search";
export const APPROVE_TEAM_USER = "/team/approveUser";

export default api;
