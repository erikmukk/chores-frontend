import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, compose } from "redux";
import { updateState } from "./redux/reducers";
/*import _ from "lodash";
import { saveState, loadState } from "./localStorage";*/

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const persistedState = loadState();
const store = createStore(updateState, composeEnhancers());
/*store.subscribe(
  _.throttle(() => {
    saveState({
      authState: store.getState().authState,
    });
  })
);*/

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
