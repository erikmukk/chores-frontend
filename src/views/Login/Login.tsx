import React, { useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import api, { CURRENT_USER, LOGIN } from "../../api";
import { useDispatch } from "react-redux";
import {
  setAuthenticationStatus,
  setCurrentUser,
  setJwt,
} from "../../redux/actions";
import { AxiosResponse } from "axios";
import { BasicUserDTO } from "../../redux/state";
import TextField from "../../components/TextField/TextField";
import { TextFieldType } from "../../components/TextField/TextFieldType";
import "./login.scss";
import * as yup from "yup";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Button from "../../components/Button/Button";

interface LoginProps {}

const loginSchema = yup.object().shape({
  username: yup.string().required(),
  password: yup.string().required(),
});

interface IFormInputs {
  username: string;
  password: string;
}

interface LocationProps {
  from: any;
}

const Login: React.FC<LoginProps> = ({}) => {
  const [authErrored, setAuthErrored] = useState<boolean>(false);
  const [authErrorMessage, setAuthErrorMessage] = useState<string>("");
  const [authInProgress, setAuthInProgress] = useState<boolean>(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const { state } = useLocation<LocationProps>();

  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(loginSchema),
  });

  const _setCurrentUser = async () => {
    await api.get(CURRENT_USER).then((resp: AxiosResponse<BasicUserDTO>) => {
      dispatch(setCurrentUser(resp.data));
    });
  };

  const handleLogin = async (data: IFormInputs) => {
    setAuthErrored(false);
    setAuthInProgress(true);
    await api
      .post(LOGIN, {
        username: data.username,
        password: data.password,
      })
      .then((resp) => {
        dispatch(setJwt(`Bearer ${resp.data.token}`));
        dispatch(setAuthenticationStatus(true));
        _setCurrentUser();
        if (state?.from?.pathname) {
          history.push(state?.from?.pathname);
        } else {
          history.push("/authedHome");
        }
      })
      .catch((error) => {
        setAuthErrored(true);
        setAuthErrorMessage(
          error?.response?.data?.message || "Something went wrong"
        );
      })
      .then(() => {
        setAuthInProgress(false);
      });
  };

  return (
    <form className={"login-form"} onSubmit={handleSubmit(handleLogin)}>
      <Controller
        name={"username"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.TEXT}
            label={"Username"}
            errored={!!errors.username}
            errorMessage={errors.username?.message}
            value={props.value}
            onChange={(e) => {
              props.onChange(e.target.value);
              setAuthErrored(false);
            }}
          />
        )}
      />
      <Controller
        name={"password"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.PASSWORD}
            label={"Password"}
            value={props.value}
            errored={!!errors.password}
            errorMessage={errors.password?.message}
            onChange={(e) => {
              props.onChange(e.target.value);
              setAuthErrored(false);
            }}
          />
        )}
      />
      {authErrored && <p className={"error-message"}>{authErrorMessage}</p>}
      <Button loading={authInProgress} text={"Login"} />
      <Link to={"/register"} className={"register-acc"}>Or create an account here</Link>
    </form>
  );
};

export default Login;
