import React from "react";
import "./authedApp.scss";
import TopBar from "../../components/TopBar/TopBar";
import UserChores from "../../components/UserChores/UserChores";

interface AuthedHomeProps {}

const AuthedApp: React.FC<AuthedHomeProps> = ({}) => {
  return (
    <div className={"authed-app"}>
      <TopBar className={"top-bar"} />
      <div className={"user-chores"}>
        <UserChores />
      </div>
    </div>
  );
};

export default AuthedApp;
