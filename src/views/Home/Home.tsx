import React from "react";
import "./home.scss";
import Button from "../../components/Button/Button";
import { useHistory } from "react-router-dom";

interface HomeProps {}

const Home: React.FC<HomeProps> = ({}) => {
  const history = useHistory();
  return (
    <div className={"home-container"}>
      <h1>Chores app</h1>
      <p>Manage your chores, create teams, have fun!</p>
      <Button text={"Sign in"} onClick={() => history.push("/login")} />
    </div>
  );
};

export default Home;
