import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import api, { REGISTER } from "../../api";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import { ApplicationState } from "../../redux/state";
import * as yup from "yup";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import TextField from "../../components/TextField/TextField";
import { TextFieldType } from "../../components/TextField/TextFieldType";
import "./register.scss";
import Button from "../../components/Button/Button";

interface RegisterProps {}

const registerSchema = yup.object().shape({
  username: yup.string().required(),
  password: yup
    .string()
    .required()
    .oneOf([yup.ref("repeatPassword"), null], "passwords must match"),
  repeatPassword: yup
    .string()
    .required("repeat password is required")
    .oneOf([yup.ref("password"), null], "passwords must match"),
  firstName: yup.string().required("first name is required"),
  lastName: yup.string().required("last name is required"),
  email: yup.string().email().required(),
});

interface IFormInputs {
  username: string;
  password: string;
  repeatPassword: string;
  firstName: string;
  lastName: string;
  email: string;
}

const Register: React.FC<RegisterProps> = ({}) => {
  const history = useHistory();
  const isAuthenticated = useSelector(
    (state: ApplicationState) => state.authState.isAuthenticated
  );
  const [isRegistering, setIsRegistering] = useState<boolean>(false);
  const [registerErrored, setRegisterErrored] = useState<boolean>(false);
  const [registerErrorMessage, setRegisterErrorMessage] = useState<string>("");
  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(registerSchema),
  });

  if (isAuthenticated) {
    history.push("/authedHome");
    return <></>;
  }

  const handleRegister = async (data: IFormInputs) => {
    setIsRegistering(true);
    await api
      .post(REGISTER, {
        username: data.username,
        password: data.password,
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName,
      })
      .then((resp) => {
        toast.success("User successfully registered");
        history.push("/login");
      })
      .catch((error) => {
        setRegisterErrored(true);
        setRegisterErrorMessage(
          error?.response?.data?.message || "Something went wrong"
        );
      })
      .then(() => setIsRegistering(false));
  };

  return (
    <form className={"register-form"} onSubmit={handleSubmit(handleRegister)}>
      <Controller
        name={"username"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.TEXT}
            label={"Username"}
            errored={!!errors.username}
            errorMessage={errors.username?.message}
            value={props.value}
            onChange={(e) => props.onChange(e.target.value)}
          />
        )}
      />
      <Controller
        name={"firstName"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.TEXT}
            label={"First name"}
            errored={!!errors.firstName}
            errorMessage={errors.firstName?.message}
            value={props.value}
            onChange={(e) => props.onChange(e.target.value)}
          />
        )}
      />
      <Controller
        name={"lastName"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.TEXT}
            label={"Last name"}
            errored={!!errors.lastName}
            errorMessage={errors.lastName?.message}
            value={props.value}
            onChange={(e) => props.onChange(e.target.value)}
          />
        )}
      />
      <Controller
        name={"email"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.EMAIL}
            label={"Email"}
            errored={!!errors.email}
            errorMessage={errors.email?.message}
            value={props.value}
            onChange={(e) => props.onChange(e.target.value)}
          />
        )}
      />
      <Controller
        name={"password"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.PASSWORD}
            label={"Password"}
            errored={!!errors.password}
            errorMessage={errors.password?.message}
            value={props.value}
            onChange={(e) => props.onChange(e.target.value)}
          />
        )}
      />
      <Controller
        name={"repeatPassword"}
        control={control}
        defaultValue={""}
        render={(props) => (
          <TextField
            type={TextFieldType.PASSWORD}
            label={"Repeat password"}
            errored={!!errors.repeatPassword}
            errorMessage={errors.repeatPassword?.message}
            value={props.value}
            onChange={(e) => props.onChange(e.target.value)}
          />
        )}
      />
      {registerErrored && (
        <p className={"error-message"}>{registerErrorMessage}</p>
      )}
      <Button loading={isRegistering} text={"Register"} />
    </form>
  );
};

export default Register;
