import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import api, { APPROVE_TEAM_USER, TEAM, USER } from "../../api";
import { AxiosResponse } from "axios";
import { BasicUserDTO, TeamDTO } from "../../redux/state";
import Button from "../../components/Button/Button";
import { toast } from "react-toastify";
import "./approveTeam.scss";

interface ApproveTeamJoinProps {}

interface ParamTypes {
  userId: string;
  teamId: string;
}

const ApproveTeamJoin: React.FC<ApproveTeamJoinProps> = ({}) => {
  const { userId, teamId } = useParams<ParamTypes>();
  const history = useHistory();
  const [teamName, setTeamName] = useState<string>("");
  const [userName, setUserName] = useState<string>("");
  const [error, setError] = useState<string>("");

  const fetchTeam = async () => {
    await api
      .get(`${USER}/one?uuid=${userId}`)
      .then((resp: AxiosResponse<BasicUserDTO>) => {
        setUserName(resp.data.username);
      })
      .catch((e) => {
        if (e.data.message) {
          setError(e.data.message);
        } else {
          setError("Something went wrong");
        }
      });
  };

  const fetchUser = async () => {
    await api
      .get(`${TEAM}/one?uuid=${teamId}`)
      .then((resp: AxiosResponse<TeamDTO>) => {
        setTeamName(resp.data.name);
      })
      .catch((e) => {
        if (e.data.message) {
          setError(e.data.message);
        } else {
          setError("Something went wrong");
        }
      });
  };

  const approveUser = async () => {
    await api
      .post(APPROVE_TEAM_USER, {
        userUuid: userId,
        teamUuid: teamId,
      })
      .then(() => {
        toast.success("User has been added to team");
        history.push("/authedHome");
      })
      .catch((e) => {
        if (e.data.message) {
          setError(e.data.message);
        } else {
          setError("Something went wrong");
        }
      });
  };

  useEffect(() => {
    fetchTeam();
    fetchUser();
  });

  return (
    <>
      {error === "" ? (
        <div className={"approve-user"}>
          <p>
            Approve user {userName} to join your team {teamName}
          </p>
          <Button className={"button"} text={"APPROVE"} onClick={approveUser} />
        </div>
      ) : (
        <div>{error}</div>
      )}
    </>
  );
};

export default ApproveTeamJoin;
