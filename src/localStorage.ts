import { AuthState } from "./redux/state";

export const loadState = () => {
  try {
    const serializedState = sessionStorage.getItem("chores-state");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (state: AuthState) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("chores-state", serializedState);
  } catch {
    // ignore write errors
  }
};
