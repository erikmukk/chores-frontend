export interface AuthState {
  isAuthenticated: boolean;
  jwt: string;
  currentUser?: BasicUserDTO;
}

export interface UserDTO {
  id: number;
  uuid: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  pendingTeams: TeamDTO[];
}

export interface BasicUserDTO {
  id: number;
  uuid: string;
  username: string;
  firstName: string;
  lastName: string;
}

export enum ChoreStatus {
  NOT_STARTED = "NOT_STARTED",
  STARTED = "STARTED",
  COMPLETED = "COMPLETED",
}

export interface StatisticsDTO {
  totalChores?: number;
  completedChores?: number;
  inProgressChores?: number;
  createdChores?: number;
  assignedChores?: number;
  teams?: number;
}

export interface ChoreDTO {
  id: number;
  name: string;
  description: string;
  createdBy: BasicUserDTO;
  assignedTo: BasicUserDTO;
  choreStatus: ChoreStatus;
}

export interface UserState {
  userTeams: TeamDTO[];
  userChores: ChoreDTO[];
  selectedTeam?: number;
}

export interface TeamDTO {
  id: number;
  uuid: string;
  name: string;
  description: string;
  admin: UserDTO;
  createdBy: UserDTO;
  members: BasicUserDTO[];
  pendingUsers: BasicUserDTO[];
  isDefaultTeam?: boolean;
}

export interface ApplicationState {
  authState: AuthState;
  userState: UserState;
}

export const defaultState: ApplicationState = {
  authState: {
    isAuthenticated: false,
    jwt: "",
  },
  userState: {
    userTeams: [],
    userChores: [],
  },
};
