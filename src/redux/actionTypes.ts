export type TYPE_SET_AUTHENTICATED = "SET_AUTHENTICATED";
export const SET_AUTHENTICATED = "SET_AUTHENTICATED";

export type TYPE_SET_JWT = "SET_JWT";
export const SET_JWT = "SET_JWT";

export type TYPE_CREATE_TEAM = "CREATE_TEAM";
export const CREATE_TEAM = "CREATE_TEAM";

export type TYPE_SET_USER_TEAMS = "SET_USER_TEAMS";
export const SET_USER_TEAMS = "SET_USER_TEAMS";

export type TYPE_ADD_USER_TEAM = "ADD_USER_TEAM";
export const ADD_USER_TEAM = "ADD_USER_TEAM";

export type TYPE_REMOVE_USER_TEAM = "REMOVE_USER_TEAM";
export const REMOVE_USER_TEAM = "REMOVE_USER_TEAM";

export type TYPE_ADD_CHORE = "ADD_CHORE";
export const ADD_CHORE = "ADD_CHORE";

export type TYPE_SET_USER_CHORES = "SET_USER_CHORES";
export const SET_USER_CHORES = "SET_USER_CHORES";

export type TYPE_SET_CURRENT_USER = "SET_CURRENT_USER";
export const SET_CURRENT_USER = "SET_CURRENT_USER";

export type TYPE_SET_SELECTED_TEAM = "SET_SELECTED_TEAM";
export const SET_SELECTED_TEAM = "SET_SELECTED_TEAM";

export type TYPE_RESET_STATE = "RESET_STATE";
export const RESET_STATE = "RESET_STATE";

export type TYPE_DELETE_CHORE = "DELETE_CHORE";
export const DELETE_CHORE = "DELETE_CHORE";
