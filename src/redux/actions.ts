import * as ActionTypes from "./actionTypes";
import { BasicUserDTO, ChoreDTO, TeamDTO } from "./state";

/**
 * Set authentication status
 */
export interface SetAuthenticationStatus {
  type: ActionTypes.TYPE_SET_AUTHENTICATED;
  authenticated: boolean;
}

export interface SetJwt {
  type: ActionTypes.TYPE_SET_JWT;
  jwt: string;
}

export interface SetUserTeams {
  type: ActionTypes.TYPE_SET_USER_TEAMS;
  teams: TeamDTO[];
}

export interface AddUserTeam {
  type: ActionTypes.TYPE_ADD_USER_TEAM;
  team: TeamDTO;
}

export interface RemoveUserTeam {
  type: ActionTypes.TYPE_REMOVE_USER_TEAM;
  teamId: number;
}

export interface AddChore {
  type: ActionTypes.TYPE_ADD_CHORE;
  chore: ChoreDTO;
}

export interface SetUserChores {
  type: ActionTypes.TYPE_SET_USER_CHORES;
  chores: ChoreDTO[];
}

export interface SetCurrentUser {
  type: ActionTypes.TYPE_SET_CURRENT_USER;
  user: BasicUserDTO;
}

export interface SetSelectedTeam {
  type: ActionTypes.TYPE_SET_SELECTED_TEAM;
  teamId: number;
}

export interface ResetState {
  type: ActionTypes.TYPE_RESET_STATE;
}

export const setJwt = (jwt: string): SetJwt => {
  return {
    type: ActionTypes.SET_JWT,
    jwt,
  };
};

export const setAuthenticationStatus = (
  authenticated: boolean
): SetAuthenticationStatus => {
  return {
    type: ActionTypes.SET_AUTHENTICATED,
    authenticated,
  };
};

export const setUserTeams = (userTeams: TeamDTO[]): SetUserTeams => {
  return {
    type: ActionTypes.SET_USER_TEAMS,
    teams: userTeams,
  };
};

export const addUserTeam = (team: TeamDTO): AddUserTeam => {
  return {
    type: ActionTypes.ADD_USER_TEAM,
    team,
  };
};

export const removeUserTeam = (teamId: number): RemoveUserTeam => {
  return {
    type: ActionTypes.REMOVE_USER_TEAM,
    teamId,
  };
};

export const addChore = (chore: ChoreDTO): AddChore => {
  return {
    type: ActionTypes.ADD_CHORE,
    chore,
  };
};

export const setUserChores = (chores: ChoreDTO[]): SetUserChores => {
  return {
    type: ActionTypes.SET_USER_CHORES,
    chores,
  };
};

export const setCurrentUser = (user: BasicUserDTO): SetCurrentUser => {
  return {
    type: ActionTypes.SET_CURRENT_USER,
    user,
  };
};

export const setSelectedTeam = (teamId: number): SetSelectedTeam => {
  return {
    type: ActionTypes.SET_SELECTED_TEAM,
    teamId,
  };
};

export const resetState = (): ResetState => {
  return {
    type: ActionTypes.RESET_STATE,
  };
};
