import { ApplicationState, defaultState } from "./state";
import {
  SetAuthenticationStatus,
  SetJwt,
  SetUserTeams,
  AddUserTeam,
  AddChore,
  SetUserChores,
  SetCurrentUser,
  SetSelectedTeam,
  RemoveUserTeam,
  ResetState,
} from "./actions";

type Action =
  | SetAuthenticationStatus
  | SetJwt
  | SetUserTeams
  | AddUserTeam
  | AddChore
  | SetUserChores
  | SetCurrentUser
  | SetSelectedTeam
  | RemoveUserTeam
  | ResetState;

const setAuthenticated = (
  state: ApplicationState,
  action: SetAuthenticationStatus
): ApplicationState => {
  return {
    ...state,
    authState: {
      ...state.authState,
      isAuthenticated: action.authenticated,
    },
  };
};

const setJwt = (state: ApplicationState, action: SetJwt): ApplicationState => {
  return {
    ...state,
    authState: {
      ...state.authState,
      jwt: action.jwt,
    },
  };
};

const setUserTeams = (
  state: ApplicationState,
  action: SetUserTeams
): ApplicationState => {
  return {
    ...state,
    userState: {
      ...state.userState,
      userTeams: action.teams,
    },
  };
};

const setUserChores = (state: ApplicationState, action: SetUserChores) => {
  return {
    ...state,
    userState: {
      ...state.userState,
      userChores: action.chores,
    },
  };
};

const addUserTeam = (state: ApplicationState, action: AddUserTeam) => {
  return {
    ...state,
    userState: {
      ...state.userState,
      userTeams: [...state.userState.userTeams, action.team],
    },
  };
};

const removeUserTeam = (state: ApplicationState, action: RemoveUserTeam) => {
  return {
    ...state,
    userState: {
      ...state.userState,
      userTeams: [
        ...state.userState.userTeams.filter(
          (team) => team.id !== action.teamId
        ),
      ],
    },
  };
};

const addChore = (state: ApplicationState, action: AddChore) => {
  return {
    ...state,
    userState: {
      ...state.userState,
      userChores: [...state.userState.userChores, action.chore],
    },
  };
};

const setCurrentUser = (state: ApplicationState, action: SetCurrentUser) => {
  return {
    ...state,
    authState: {
      ...state.authState,
      currentUser: action.user,
    },
  };
};

const setSelectedTeam = (state: ApplicationState, action: SetSelectedTeam) => {
  return {
    ...state,
    userState: {
      ...state.userState,
      selectedTeam: action.teamId,
    },
  };
};

const resetState = () => {
  return defaultState;
};

export const updateState = (
  state: ApplicationState = defaultState,
  action: Action
) => {
  switch (action.type) {
    case "SET_AUTHENTICATED":
      return setAuthenticated(state, action);
    case "SET_JWT":
      return setJwt(state, action);
    case "SET_USER_TEAMS":
      return setUserTeams(state, action);
    case "ADD_USER_TEAM":
      return addUserTeam(state, action);
    case "ADD_CHORE":
      return addChore(state, action);
    case "SET_USER_CHORES":
      return setUserChores(state, action);
    case "SET_CURRENT_USER":
      return setCurrentUser(state, action);
    case "SET_SELECTED_TEAM":
      return setSelectedTeam(state, action);
    case "REMOVE_USER_TEAM":
      return removeUserTeam(state, action);
    case "RESET_STATE":
      return resetState();
    default:
      return state;
  }
};
