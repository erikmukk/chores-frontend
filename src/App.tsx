import React, { useEffect } from "react";
import Home from "./views/Home/Home";
import Login from "./views/Login/Login";
import Register from "./views/Register/Register";
import { Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSelector } from "react-redux";
import { ApplicationState } from "./redux/state";
import api from "./api";
import "./App.scss";
import AuthedApp from "./views/AuthedApp/AuthedApp";
import ApproveTeamJoin from "./views/ApproveTeamJoin/ApproveTeamJoin";

function App() {
  const jwt = useSelector((state: ApplicationState) => state.authState.jwt);
  const authed = useSelector(
    (state: ApplicationState) => state.authState.isAuthenticated
  );
  useEffect(() => {
    api.defaults.headers.Authorization = jwt;
  });

  // @ts-ignore
  const ProtectedRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={(props) =>
        authed ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }
    />
  );

  return (
    <div className="app">
      <Switch>
        <Route path={"/"} component={Home} exact />
        <ProtectedRoute path={"/authedHome"} component={AuthedApp} exact />
        <ProtectedRoute
          path={"/approveTeamJoin/:teamId/:userId"}
          component={ApproveTeamJoin}
        />
        <Route path={"/login"} component={Login} />
        <Route path={"/register"} component={Register} />
      </Switch>
      <ToastContainer
        position="top-center"
        pauseOnFocusLoss={false}
        pauseOnHover={false}
      />
    </div>
  );
}

export default App;
