/*https://coolors.co/2e5266-6e8898-9fb1bc-d3d0cb-e2c044*/
export enum Colors {
  BACKGROUND = "#bdbdbd",
  DARK_GREEN = "#006442",
  LIGHT_GREY = "#3e3e3e",
  GREEN = "#26A65B",
  DARK_GREY = "#272727",
  ORANGE = "#FF652F",
  STICKY_YELLOW = "#FFFF88",
}
