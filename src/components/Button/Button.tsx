import React from "react";
import "./button.scss";
import clsx from "clsx";

interface ButtonProps {
  text: string;
  loading?: boolean;
  onClick?: (e: React.MouseEvent) => void;
  className?: string;
  error?: boolean;
}

const Button: React.FC<ButtonProps> = ({
  text,
  onClick,
  loading,
  className,
  error,
}) => {
  return (
    <button
      className={clsx(
        "custom-button",
        error ? "__error" : "__success",
        loading && "__loading",
        className
      )}
      onClick={onClick}
      disabled={loading}
    >
      {!loading ? (
        text
      ) : (
        <div className={"loading-circles"}>
          <div className={"loading-circle"}></div>
          <div className={"loading-circle"}></div>
          <div className={"loading-circle"}></div>
        </div>
      )}
    </button>
  );
};

export default Button;
