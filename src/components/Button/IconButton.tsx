import React from "react";
import "./iconButton.scss";

interface IconButtonProps {
  icon: React.ReactNode;
  onClick?: () => void;
}

const IconButton: React.FC<IconButtonProps> = ({ icon, onClick }) => {
  return (
    <button onClick={onClick} className={"icon-button"}>
      {icon}
    </button>
  );
};

export default IconButton;
