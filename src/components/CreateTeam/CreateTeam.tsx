import React, { useState } from "react";
import * as yup from "yup";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import "./createTeam.scss";
import TextField from "../TextField/TextField";
import { TextFieldType } from "../TextField/TextFieldType";
import Button from "../Button/Button";
import TextArea from "../TextArea/TextArea";
import api, { TEAM } from "../../api";
import { AxiosResponse } from "axios";
import { TeamDTO } from "../../redux/state";
import { useDispatch } from "react-redux";
import { addUserTeam } from "../../redux/actions";
import { toast } from "react-toastify";

interface CreateTeamProps {
  onClose: () => void;
}

const registerSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
});

interface IFormInputs {
  name: string;
  description: string;
}

const CreateTeam: React.FC<CreateTeamProps> = ({ onClose }) => {
  const [isCreatingTeam, setIsCreatingTeam] = useState<boolean>(false);
  const dispatch = useDispatch();
  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(registerSchema),
  });

  const handleCreateTeam = async (data: IFormInputs) => {
    setIsCreatingTeam(true);
    await api
      .post(TEAM, { name: data.name, description: data.description })
      .then((resp: AxiosResponse<TeamDTO>) => {
        dispatch(addUserTeam(resp.data));
        toast.success("Team successfully created");
      })
      .catch(() => {})
      .then(() => {
        setIsCreatingTeam(false);
        onClose();
      });
  };

  return (
    <div>
      <form
        className={"create-team-form"}
        onSubmit={handleSubmit(handleCreateTeam)}
      >
        <Controller
          name={"name"}
          control={control}
          defaultValue={""}
          render={(props) => (
            <TextField
              type={TextFieldType.TEXT}
              label={"Team name"}
              errored={!!errors.name}
              errorMessage={errors.name?.message}
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
            />
          )}
        />
        <Controller
          name={"description"}
          control={control}
          defaultValue={""}
          render={(props) => (
            <TextArea
              label={"Team description"}
              errored={!!errors.description}
              errorMessage={errors.description?.message}
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
              rows={8}
            />
          )}
        />
        <Button loading={isCreatingTeam} text={"Create team"} />
      </form>
    </div>
  );
};

export default CreateTeam;
