import React, { useEffect, useState } from "react";
import { AxiosResponse } from "axios";
import { useDispatch, useSelector } from "react-redux";
import AddChore from "./AddChore/AddChore";
import ChoreCard from "./ChoreCard/ChoreCard";
import api, { CHORE } from "../../api";
import {
  ApplicationState,
  BasicUserDTO,
  ChoreDTO,
  ChoreStatus,
  TeamDTO,
} from "../../redux/state";
import "./userChores.scss";
import { setUserChores } from "../../redux/actions";

interface UserChoresProps {}

const UserChores: React.FC<UserChoresProps> = ({}) => {
  const dispatch = useDispatch();
  const [currentTeam, setCurrentTeam] = useState<TeamDTO | undefined>(
    undefined
  );

  const userTeams: TeamDTO[] = useSelector(
    (state: ApplicationState) => state.userState.userTeams
  );

  const userChores: ChoreDTO[] = useSelector(
    (state: ApplicationState) => state.userState.userChores
  );

  const currentUser: BasicUserDTO | undefined = useSelector(
    (state: ApplicationState) => state.authState.currentUser
  );

  const selectedTeamId: number | undefined = useSelector(
    (state: ApplicationState) => state.userState.selectedTeam
  );

  const [firstColumn, setFirstColumn] = useState<ChoreDTO[]>([]);
  const [secondColumn, setSecondColumn] = useState<ChoreDTO[]>([]);
  const [thirdColumn, setThirdColumn] = useState<ChoreDTO[]>([]);

  useEffect(() => {
    if (selectedTeamId) {
      fetchUserChores();
      setCurrentTeam(userTeams.find((team) => team.id === selectedTeamId));
    }
  }, [selectedTeamId]);

  useEffect(() => {
    const first: ChoreDTO[] = [];
    const second: ChoreDTO[] = [];
    const third: ChoreDTO[] = [];
    let counter = 0;
    userChores.forEach((chore) => {
      if (counter === 0) {
        second.push(chore);
      } else if (counter === 1) {
        third.push(chore);
      } else {
        first.push(chore);
      }
      counter++;
      if (counter === 3) {
        counter = 0;
      }
    });
    setFirstColumn(first);
    setSecondColumn(second);
    setThirdColumn(third);
  }, [userChores]);

  const sortChores = (a: ChoreDTO, b: ChoreDTO) => {
    if (
      a.choreStatus === ChoreStatus.NOT_STARTED &&
      (b.choreStatus === ChoreStatus.STARTED ||
        b.choreStatus === ChoreStatus.COMPLETED)
    ) {
      return -1;
    }
    if (a.choreStatus === b.choreStatus) {
      return 0;
    }
    return 1;
  };

  const fetchUserChores = async () => {
    await api
      .get(`${CHORE}/team/${selectedTeamId}`)
      .then((resp: AxiosResponse<ChoreDTO[]>) => {
        dispatch(setUserChores(resp.data.sort(sortChores)));
      });
  };

  const onDelete = (chore: ChoreDTO) => {
    const newChores = userChores.filter((_chore) => _chore.id !== chore.id);
    dispatch(setUserChores(newChores.sort(sortChores)));
  };

  const onChange = (chore: ChoreDTO) => {
    const newChores = userChores.map((_chore) => {
      if (chore.id === _chore.id) {
        return chore;
      }
      return _chore;
    });
    dispatch(setUserChores(newChores.sort(sortChores)));
  };

  return (
    <div className={"container"}>
      <h2>Team: {currentTeam?.name}</h2>
      <div className={"chores-container"}>
        <div className={"chores-column"}>
          <AddChore user={currentUser} team={selectedTeamId} />
          {firstColumn.map((chore) => (
            <ChoreCard
              handleDelete={onDelete}
              handleChange={onChange}
              key={`col1-${chore.id}`}
              chore={chore}
            />
          ))}
        </div>
        <div className={"chores-column"}>
          {secondColumn.map((chore) => (
            <ChoreCard
              handleDelete={onDelete}
              handleChange={onChange}
              key={`col2-${chore.id}`}
              chore={chore}
            />
          ))}
        </div>
        <div className={"chores-column"}>
          {thirdColumn.map((chore) => (
            <ChoreCard
              handleDelete={onDelete}
              handleChange={onChange}
              key={`col3-${chore.id}`}
              chore={chore}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default UserChores;
