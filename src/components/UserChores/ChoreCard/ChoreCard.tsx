import React from "react";
import clsx from "clsx";
import { ChoreDTO, ChoreStatus } from "../../../redux/state";
import TeamCircle from "../../TeamBar/TeamCircle/TeamCircle";
import "./choreCard.scss";
import "../sharedChoreCard.scss";
import { MdDelete, TiTick } from "react-icons/all";
import IconButton from "../../Button/IconButton";
import api, { CHORE } from "../../../api";
import { AxiosResponse } from "axios";

interface ChoreCardProps {
  chore: ChoreDTO;
  handleDelete: (chore: ChoreDTO) => void;
  handleChange: (chore: ChoreDTO) => void;
}

const ChoreCard: React.FC<ChoreCardProps> = ({
  chore,
  handleDelete,
  handleChange,
}) => {
  const changeChore = async (action: string) => {
    if (action === "tick") {
      await api
        .put(`${CHORE}/${chore.id}`, {
          choreStatus:
            chore.choreStatus === ChoreStatus.NOT_STARTED
              ? ChoreStatus.STARTED
              : ChoreStatus.COMPLETED,
        })
        .then((resp: AxiosResponse<ChoreDTO>) => handleChange(resp.data));
    } else if (action === "delete") {
      await api.delete(`${CHORE}/${chore.id}`).then(() => {
        handleDelete(chore);
      });
    }
  };
  const choreStatusClass = () => {
    switch (chore.choreStatus) {
      case ChoreStatus.COMPLETED:
        return "__completed";
      case ChoreStatus.NOT_STARTED:
        return "__not-started";
      case ChoreStatus.STARTED:
        return "__started";
      default:
        return "";
    }
  };

  return (
    <div className={clsx("chore-card", choreStatusClass())}>
      <div className={"chore-header"}>
        <TeamCircle onTeamBar={false} user={chore.assignedTo} />
        <div className={"chore-data"}>
          <span>{chore.name}</span>
          <div className={"chore-user-data"}>
            <span className={"by-user-name"}>
              By: {chore.createdBy.firstName} {chore.createdBy.lastName}
            </span>
            <span className={"by-user-name"}>
              For: {chore.assignedTo.firstName} {chore.assignedTo.lastName}
            </span>
          </div>
        </div>
      </div>
      <div className={"chore-description"}>
        <p>{chore.description}</p>
      </div>
      <div className={"chore-actions"}>
        {chore.choreStatus === ChoreStatus.NOT_STARTED && (
          <>
            <IconButton
              onClick={() => changeChore("delete")}
              icon={<MdDelete />}
            />
            <IconButton onClick={() => changeChore("tick")} icon={<TiTick />} />
          </>
        )}
        {chore.choreStatus === ChoreStatus.COMPLETED && (
          <IconButton
            onClick={() => changeChore("delete")}
            icon={<MdDelete />}
          />
        )}
        {chore.choreStatus === ChoreStatus.STARTED && (
          <IconButton onClick={() => changeChore("tick")} icon={<TiTick />} />
        )}
      </div>
    </div>
  );
};

export default ChoreCard;
