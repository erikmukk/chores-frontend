import React, { useEffect, useState } from "react";
import "./addChore.scss";
import "../sharedChoreCard.scss";
import TeamCircle from "../../TeamBar/TeamCircle/TeamCircle";
import { BasicUserDTO, ChoreDTO, TeamDTO } from "../../../redux/state";
import api, { CHORE, TEAM } from "../../../api";
import { AxiosResponse } from "axios";
import { AiOutlinePlusCircle } from "react-icons/all";
import { useDispatch } from "react-redux";
import { addChore as addChoreAction } from "../../../redux/actions";
import clsx from "clsx";

interface AddChoreProps {
  user?: BasicUserDTO;
  team?: number;
}

interface UserOption {
  label: string;
  value: number;
}

const AddChore: React.FC<AddChoreProps> = ({ user, team }) => {
  const dispatch = useDispatch();
  const [teamUsers, setTeamUsers] = useState<UserOption[]>([]);
  const [description, setDescription] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [selectedUser, setSelectedUser] = useState(0);
  const fetchTeamUsers = async () => {
    await api.get(`${TEAM}/${team}`).then((resp: AxiosResponse<TeamDTO>) => {
      const options: UserOption[] = resp.data.members.map((_user) => {
        return {
          label: `${_user.username} - ${_user.firstName} ${_user.lastName}`,
          value: _user.id,
        };
      });
      if (options.length === 1) {
        setSelectedUser(options[0].value);
      }
      setTeamUsers(options);
    });
  };
  useEffect(() => {
    if (team) {
      fetchTeamUsers();
    }
  }, [team]);

  const resetFields = () => {
    setDescription("");
    setTitle("");
  };

  const addChore = async () => {
    await api
      .post(CHORE, {
        name: title,
        description: description,
        assignedTo: selectedUser,
        targetTeam: team,
      })
      .then((resp: AxiosResponse<ChoreDTO>) => {
        dispatch(addChoreAction(resp.data));
        resetFields();
      });
  };

  return (
    <div className={"chore-card"}>
      <div className={"add-chore-header"}>
        <TeamCircle onTeamBar={false} user={user} />
        <div className={"chore-data"}>
          <input
            className={"chore-title"}
            placeholder={"title"}
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <span className={"user-name"}>
            {user?.firstName} {user?.lastName}
          </span>
        </div>
      </div>
      <div className={"add-chore-select-team"}>
        <span className={"assign-to"}>Assign to:</span>
        {teamUsers.length > 1 ? (
          <select
            value={selectedUser}
            onChange={(e) => setSelectedUser(Number(e.target.value))}
          >
            {teamUsers.map((user) => {
              return (
                <option
                  className={"assign-option"}
                  key={user.value}
                  value={user.value}
                >
                  {user.label}
                </option>
              );
            })}
          </select>
        ) : (
          <span className={"assign-to"}>myself</span>
        )}
      </div>
      <div className={"add-chore-description"}>
        <textarea
          rows={4}
          className={"chore-description-input"}
          placeholder={"Description"}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </div>
      {title.length > 0 && description.length > 0 && (
        <div
          className={clsx(
            "submit-chore",
            title.length === 0 && description.length === 0 && "collapsed"
          )}
        >
          <button onClick={addChore}>
            <AiOutlinePlusCircle />
          </button>
        </div>
      )}
    </div>
  );
};

export default AddChore;
