import React, { ReactNode, useEffect, useRef } from "react";
import "./modal.scss";
import Button from "../Button/Button";

interface ModalProps {
  children?: ReactNode;
  open: boolean;
  onClose: () => void;
}

const Modal: React.FC<ModalProps> = ({ children, open, onClose }) => {
  const useOutsideClickCapturer = (ref: any) => {
    useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      const handleClickOutside = (event: any) => {
        if (ref.current && !ref.current.contains(event.target)) {
          onClose();
        }
      };
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  };
  const ref = useRef(null);
  useOutsideClickCapturer(ref);
  if (!open) {
    return null;
  }
  return (
    <div className={"modal"}>
      <div ref={ref} className={"modal-body"}>
        <div className={"modal-content"}>{children}</div>
        <div className={"modal-buttons"}>
          <Button
            className={"modal-close"}
            text={"CLOSE"}
            onClick={() => onClose()}
          />
        </div>
      </div>
    </div>
  );
};

export default Modal;
