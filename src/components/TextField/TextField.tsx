import React from "react";
import "./textField.scss";
import { TextFieldType } from "./TextFieldType";
import clsx from "clsx";

interface TextFieldProps {
  type: TextFieldType;
  label: string;
  value: string | number;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  errored: boolean;
  errorMessage: string | null;
  className?: string;
}

const TextField: React.FC<TextFieldProps> = ({
  type,
  label,
  value,
  onChange,
  errored,
  errorMessage,
  className,
}) => {
  return (
    <>
      <input
        className={clsx("custom-input", errored && "__errored", className)}
        type={type}
        id={label}
        name={label}
        onChange={onChange}
        value={value}
        placeholder={label}
      />
      {errorMessage && <p className={"error-message"}>{errorMessage}</p>}
    </>
  );
};

export default TextField;
