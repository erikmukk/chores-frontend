export enum TextFieldType {
  PASSWORD = "password",
  EMAIL = "email",
  TEXT = "text",
  NUMBER = "number",
}
