import React from "react";
import "./createTeamCircle.scss";
import clsx from "clsx";

interface CreateTeamCircleProps {
  onClick: () => void;
}

const CreateTeamCircle: React.FC<CreateTeamCircleProps> = ({ onClick }) => {
  return (
    <div className={clsx("team-circle-join")} onClick={() => onClick()}>
      <span className={"create-team"}>+</span>
    </div>
  );
};

export default CreateTeamCircle;
