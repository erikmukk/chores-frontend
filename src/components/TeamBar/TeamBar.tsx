import React, { useEffect, useState } from "react";
import "./teamBar.scss";
import TeamCircle from "./TeamCircle/TeamCircle";
import clsx from "clsx";
import api, { CURRENT_USER_TEAMS } from "../../api";
import { AxiosResponse } from "axios";
import { ApplicationState, TeamDTO } from "../../redux/state";
import { useDispatch, useSelector } from "react-redux";
import { setSelectedTeam, setUserTeams } from "../../redux/actions";
import CreateTeamCircle from "./CreateTeamCircle/CreateTeamCircle";
import Modal from "../Modal/Modal";
import CreateTeam from "../CreateTeam/CreateTeam";

interface TeamBarProps {
  className?: string;
}

const TeamBar: React.FC<TeamBarProps> = ({ className }) => {
  const [createTeamModalOpen, setCreateTeamModalOpen] = useState<boolean>(
    false
  );
  const dispatch = useDispatch();
  const teams: TeamDTO[] = useSelector(
    (state: ApplicationState) => state.userState.userTeams
  );
  const selectedTeamId: number | undefined = useSelector(
    (state: ApplicationState) => state.userState.selectedTeam
  );

  useEffect(() => {
    fetchUserTeams();
  }, []);

  useEffect(() => {
    if (teams.length > 0) {
      dispatch(setSelectedTeam(teams[0].id));
    }
  }, [teams]);

  const fetchUserTeams = async () => {
    await api.get(CURRENT_USER_TEAMS).then((resp: AxiosResponse<TeamDTO[]>) => {
      dispatch(
        setUserTeams(resp.data.sort((elem) => (elem.isDefaultTeam ? 1 : 0)))
      );
    });
  };
  const handleTeamSelection = (event: React.MouseEvent, id: number) => {
    dispatch(setSelectedTeam(id));
  };

  return (
    <div className={clsx(className, "team-bar")}>
      <Modal
        open={createTeamModalOpen}
        onClose={() => setCreateTeamModalOpen(false)}
        children={<CreateTeam onClose={() => setCreateTeamModalOpen(false)} />}
      />
      {teams.map((team) => {
        return (
          <TeamCircle
            onTeamBar
            key={team.id}
            team={team}
            active={selectedTeamId === team.id}
            onClick={handleTeamSelection}
          />
        );
      })}
      <CreateTeamCircle onClick={() => setCreateTeamModalOpen(true)} />
    </div>
  );
};

export default TeamBar;
