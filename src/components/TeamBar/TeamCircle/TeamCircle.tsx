import React from "react";
import "./teamCircle.scss";
import clsx from "clsx";
import { BasicUserDTO, TeamDTO } from "../../../redux/state";
import _ from "lodash";

interface TeamCircleProps {
  active?: boolean;
  team?: TeamDTO;
  onClick?: (event: React.MouseEvent, id: number) => void;
  user?: BasicUserDTO;
  onTeamBar: boolean;
}

const TeamCircle: React.FC<TeamCircleProps> = ({
  active,
  team,
  onClick,
  onTeamBar,
  user,
}) => {
  return (
    <div
      className={clsx("team-circle", active && "__active")}
      onClick={(e: React.MouseEvent) => onClick && team && onClick(e, team.id)}
    >
      {onTeamBar && team && (
        <span className={"team-name"}>
          {_.truncate(team.name, { length: 2, omission: "" })}
        </span>
      )}
      {!onTeamBar && user && (
        <span className={"username"}>
          {_.truncate(user.username, { length: 2, omission: "" })}
        </span>
      )}
    </div>
  );
};

export default TeamCircle;
