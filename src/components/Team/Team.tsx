import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import api, { TEAM } from "../../api";
import { AxiosResponse } from "axios";
import Button from "../Button/Button";
import { ApplicationState, TeamDTO } from "../../redux/state";
import { removeUserTeam } from "../../redux/actions";
import "./team.scss";

interface TeamProps {
  id?: number;
}

const Team: React.FC<TeamProps> = ({ id }) => {
  const dispatch = useDispatch();
  const [team, setTeam] = useState<TeamDTO | null>(null);
  const [isMemberOfTeam, setIsMemberOfTeam] = useState<boolean>(true);
  const [isPendingApproval, setIsPendingApproval] = useState<boolean>(false);
  const currentUser = useSelector(
    (state: ApplicationState) => state.authState.currentUser
  );
  const fetchTeam = async () => {
    await api.get(`${TEAM}/${id}`).then((resp: AxiosResponse<TeamDTO>) => {
      setTeam(resp.data);
      setIsPendingApproval(
        resp.data.pendingUsers.some((user) => user.id === currentUser?.id)
      );
    });
  };

  const joinTeam = async () => {
    await api
      .put(`${TEAM}/${team?.id}/join`, {
        userId: currentUser?.id,
      })
      .then((resp: AxiosResponse<TeamDTO>) => {
        setIsPendingApproval(
          resp.data.pendingUsers.some((user) => user.id === currentUser?.id)
        );
      });
  };
  const leaveTeam = async () => {
    await api
      .put(`${TEAM}/${team?.id}/leave`, {
        userId: currentUser?.id,
      })
      .then((resp: AxiosResponse<TeamDTO>) => {
        dispatch(removeUserTeam(resp.data.id));
        setIsMemberOfTeam(false);
      });
  };

  useEffect(() => {
    if (id) {
      fetchTeam();
    }
  }, [id]);

  useEffect(() => {
    if (team) {
      setIsMemberOfTeam(
        team.members.some((teamMember) => teamMember.id === currentUser?.id)
      );
    }
  }, [team]);

  if (!id) {
    return null;
  }
  if (!team) {
    return <div>Loading team...</div>;
  }

  return (
    <div className={"team-container"}>
      <div className={"team-name"}>
        <h2>{team.name}</h2>
      </div>
      <div className={"team-description"}>
        <p>{team.description}</p>
      </div>
      <div className={"team-admin"}>
        <p>
          Admin: {team.admin.username} ({team.admin.firstName}{" "}
          {team.admin.lastName})
        </p>
      </div>
      {team.admin.id !== currentUser?.id && (
        <div className={"team-actions"}>
          {isMemberOfTeam ? (
            <Button text={"LEAVE TEAM"} error onClick={leaveTeam} />
          ) : isPendingApproval ? (
            <span>Pending approval to join team</span>
          ) : (
            <Button text={"JOIN TEAM"} onClick={joinTeam} />
          )}
        </div>
      )}
    </div>
  );
};

export default Team;
