import React, { useRef, useState } from "react";
import clsx from "clsx";
import TextField from "../TextField/TextField";
import TeamBar from "../TeamBar/TeamBar";
import { TextFieldType } from "../TextField/TextFieldType";
import "./topBar.scss";
import _ from "lodash";
import api, { TEAM_SEARCH } from "../../api";
import { TeamDTO } from "../../redux/state";
import { AxiosResponse } from "axios";
import Modal from "../Modal/Modal";
import Team from "../Team/Team";
import { GrLogout } from "react-icons/all";
import { useDispatch } from "react-redux";
import { resetState } from "../../redux/actions";
import { useHistory } from "react-router-dom";

interface TopBarProps {
  className: string;
}

const TopBar: React.FC<TopBarProps> = ({ className }) => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [searchResult, setSearchResult] = useState<TeamDTO[]>([]);
  const [teamModalOpen, setTeamModalOpen] = useState<boolean>(false);
  const [modalTeamId, setModalTeamId] = useState<number | undefined>(undefined);
  const dispatch = useDispatch();
  const history = useHistory();

  const searchTeams = async (q: string) => {
    await api
      .get(`${TEAM_SEARCH}?searchTerm=${q}`)
      .then((resp: AxiosResponse<TeamDTO[]>) => {
        setSearchResult(
          resp.data.filter((elem) => elem.isDefaultTeam !== true)
        );
      });
  };

  const debouncedSearch = useRef(_.debounce((q) => searchTeams(q), 500))
    .current;

  const handleSearch = (e: string) => {
    setSearchTerm(e);
    debouncedSearch(e);
  };

  const goToTeam = (id: number) => {
    setSearchTerm("");
    setSearchResult([]);
    setTeamModalOpen(true);
    setModalTeamId(id);
  };

  const logout = () => {
    dispatch(resetState());
    history.push("/");
  };

  return (
    <div className={clsx(className, "top-util-bar")}>
      <Modal
        open={teamModalOpen}
        onClose={() => setTeamModalOpen(false)}
        children={<Team id={modalTeamId} />}
      />
      <TextField
        className={"team-search"}
        type={TextFieldType.TEXT}
        label={"Search"}
        value={searchTerm}
        onChange={(e) => handleSearch(e.target.value)}
        errored={false}
        errorMessage={""}
      />
      {searchResult.length > 0 && (
        <div className={"search-result"}>
          {searchResult.map((team) => {
            return (
              <div
                key={team.id}
                className={"result-item"}
                onClick={() => goToTeam(team.id)}
              >
                {team.name}
              </div>
            );
          })}
        </div>
      )}
      <TeamBar className={"team-overview"}>teams</TeamBar>
      <div className={"logout"}>
        <GrLogout onClick={logout} />
      </div>
    </div>
  );
};

export default TopBar;
