import React from "react";
import "./textArea.scss";
import clsx from "clsx";

interface TextAreaProps {
  label: string;
  value: string | number;
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
  errored: boolean;
  errorMessage: string | null;
  className?: string;
  rows?: number;
}

const TextArea: React.FC<TextAreaProps> = ({
  label,
  value,
  onChange,
  errored,
  errorMessage,
  className,
  rows,
}) => {
  return (
    <>
      <textarea
        className={clsx("custom-textarea", errored && "__errored", className)}
        id={label}
        name={label}
        onChange={onChange}
        value={value}
        placeholder={label}
        rows={rows || 4}
      />
      {errorMessage && <p className={"error-message"}>{errorMessage}</p>}
    </>
  );
};

export default TextArea;
